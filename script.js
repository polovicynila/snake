let canvas = document.getElementById('canvas')
const context = canvas.getContext('2d')
console.log(context)
const ROWS = 10
const COLUMNS = 10
const CELL_SIZE = 50
const CELL_MARGIN = 3
const GAME_PADDING = 10
const FOOD_COLOR = 'green'
const BAD_FOOD_COLOR = 'red'
const SNAKE_COLOR = 'yellow'
const FREE_COLOR = 'rgb(240, 240, 240)'
const START_COOLDOWN = 400

canvas.width = CELL_SIZE * COLUMNS + (COLUMNS - 1) * CELL_MARGIN + 2 * GAME_PADDING
canvas.height = CELL_SIZE * ROWS + (ROWS - 1) * CELL_MARGIN + 2 * GAME_PADDING

let map = createGameMap(COLUMNS, ROWS)
getRandomFreeCell(map).food = true
getRandomFreeCell(map).badFood = true

const cell = getRandomFreeCell(map)
let snake = [cell]
cell.snake = true
let play = true
let prevTick = 0
let cooldown = START_COOLDOWN
let snakeDirect = 'up'
let nextSnakeDirect = 'up'

requestAnimationFrame(loop)

function loop(timestamp) {
    requestAnimationFrame(loop)


    clearCanvas()


    if (prevTick + cooldown <= timestamp && play) {
        prevTick = timestamp
        snakeDirect = nextSnakeDirect
        moveSnake()
        const head = snake[0]
        const tail = snake[snake.length - 1]

        if (head.food) {
            head.food = false;
            snake.push(tail);
            getRandomFreeCell(map).food = true
        }
        else {
            let isEnd = false
            if (head.badFood) {
                head.badfood = false
                snake.pop()
                if (snake.length !== 0) {
                    getRandomFreeCell(map).badFood = true
                }
                else {
                    isEnd = true
                }

            }

            for (let i = 1; i < snake.length; i++) {
                if (snake[i] === snake[0]) {
                    isEnd = true
                    break
                }
            }

            if (isEnd) {
                play = false
            }
        }
    } drawGameMap(map)
    showState()
    if (!play) {
        drawPaused()
    }
}

document.addEventListener('keydown', function (event) {
    if (event.key === 'W') {
        console.log('up')
        if (snake.lenght === 1 || snakeDirect === 'left' || snakeDirect === 'right') {
            nextSnakeDirect = 'up'
        }
    }

    else if (event.key === 'S') {
        console.log('down')
        if (snake.lenght === 1 || snakeDirect === 'left' || snakeDirect === 'right') {
            nextSnakeDirect = 'down'
        }
    }

    else if (event.key === 'A') {
        console.log('left')
        if (snake.lenght === 1 || snakeDirect === 'up' || snakeDirect === 'down') {
            nextSnakeDirect = 'left'
        }
    }

    else if (event.key === 'D') {
        console.log('right')
        if (snake.lenght === 1 || snakeDirect === 'up' || snakeDirect === 'down') {
            nextSnakeDirect = 'right'
        }
    }

    else if (event.key === 'Enter') {
        console.log('Enter')
        if (play) {
            return
        }
        init()
    }

})